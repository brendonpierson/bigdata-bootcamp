---
layout: SpecialPage
---
# About Us

<!--[sunlab-team](images/avatar/aboutus.jpg "Sunlab team")-->

![sunlab-team](images/avatar/aboutus.jpg "Sunlab team")

## WebEx Office Hour

We will host office hour(1 hour per person) through WebEx or bluejean **every weekday**. See below table for detailed schedule

| Photo| Name|Time (EST)             | Location or Web Link |
| :-------------: | :-------------: | ---------------- | --------------------------------------------------------------------------------------|
|![minipic](images/avatar/Jimeng.png)   |  Jimeng Sun, instructor jsun<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;cc.gatech.edu     |      |       Request by email         |
|![minipic](images/avatar/andys.png )| Andy Soobrian, TA asoobian3<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Mon 6PM | [https://gatech.webex.com/gatech/j.php?MTID=m5b49fe73fec154663176adf45f7b6afd](https://gatech.webex.com/gatech/j.php?MTID=m5b49fe73fec154663176adf45f7b6afd)
|![minipic](images/avatar/MingLiu.jpg) | Ming Liu , PhD,  TA mliu302<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Wed 10AM |<https://gatech.webex.com/meet/mliu302>
|![minipic](images/avatar/Balaji.png) |     Balaji Sundaresan, TA bsundaresan3<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu | Thu 9PM | [https://gatech.webex.com/meet/bsundaresan3](https://gatech.webex.com/meet/bsundaresan3)
|![minipic](images/avatar/SungtaeAn.jpg) | Sungtae An, Sunlab PhD,  TA  stan84<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Mon 6PM | <https://gatech.webex.com/meet/san37>
|![minipic](images/avatar/YuJing.jpg) | Yu Jing,  TA yujing<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Fri 10AM | <https://gatech.webex.com/meet/yjing43>
|![minipic](images/avatar/yanbo.png) | Yanbo Xu, Sunlab PhD,  TA yxu465<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Wed 3:30PM | <https://gatech.webex.com/meet/yxu465>
|![minipic](images/avatar/siddharth.png) | Siddharth Biswal, Sunlab PhD, TA sbiswal7<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Thu 11AM | <https://gatech.webex.com/gatech/j.php?MTID=m90a6a0cc5ccfe87f3c15a6a578cfb068>