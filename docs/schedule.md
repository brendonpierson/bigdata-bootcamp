---
layout: SpecialPage
---
# Course Syllabus

[[toc]]

## Overview

<!--Both on-campus and OMS student should watch  Udacity course videos. On-campus student should watch video before class. Deliverable due dates apply to both OMS and on-campus student.-->
Students should watch Udacity course videos according to the following schedule. It is **recommended** for students to do lab sessions on the schedule by yourself **as early as possible** since some of homework may cover the lab materials scheduled later than the homework.

## Schedule

<!-- **Guest lecture by Noemie Elhadad (Columbia)**   -->

| Week # | Dates     |  Video lessons                       | Lab                              | Deliverable Due                                                          | 
|--------|-----------|---------------------------------------------------------------------|-------------------------------------|----------------------------------|--------------------------------------------------------------------------| 
| 1      | Aug 20-24  |  [1. Intro to Big Data Analytics](https://www.udacity.com/course/viewer#!/c-ud758/l-6311012028), [2. Course Overview](https://www.udacity.com/course/viewer#!/c-ud758/l-5046828066)     |  [Scala Basic](/spark/scala-basic.html)                      |                                                                          | 
| 2      | Aug 27-31 |  [3. Predictive Modeling](https://www.udacity.com/course/viewer#!/c-ud758/l-5484251492)              |   [Hadoop & HDFS Basics](/hadoop/hdfs-basic.html)                               |    HW1 Due (Sep 2)                                                                       | 
| 3      | Sep 3-7 |  [4.MapReduce](https://www.udacity.com/course/viewer#!/c-ud758/l-6298155413)& [HBase](/hadoop/hadoop-hbase.html)                  |                                  |                                                           | 
| 4      | Sep 10-14 |  [5.Classification evaluation metrics](https://www.udacity.com/course/viewer#!/c-ud758/l-5505090946), [6.Classification ensemble methods](https://www.udacity.com/course/viewer#!/c-ud758/l-5615268587) |   [Hadoop Pig](/hadoop/hadoop-pig.html) & [Hive](/hadoop/hadoop-hive.html)                               |   HW2 Due (Sep 16)                                                                        | 
| 5      | Sep 17-21  |  [7. Phenotyping](https://www.udacity.com/course/viewer#!/c-ud758/l-6363218753), [8. Clustering](https://www.udacity.com/course/viewer#!/c-ud758/l-6343118554)                      |  [Spark Basic](/spark/spark-basic.html), [Spark SQL](/spark/spark-sql.html)                                |                                                                          | 
| 6      | Sep 24-28 |  [9. Spark](https://www.udacity.com/course/viewer#!/c-ud758/l-6376189383/m-6861062716)                            |   [Spark Application](/spark/spark-application.html) & [Spark MLlib](/spark/spark-mllib.html)                               |    HW3 Due & Project Group Formation & Project Requirements Release (proposal/draft/final) (Sep 30)                                                                      | 
| 7      | Oct 1-5 |  [10. Medical ontology](https://www.udacity.com/course/viewer#!/c-ud758/l-6370309670)                 |  [NLP Lab](/nlp/solr.html)                                |                                                                          | 
| 8      | Oct 8-12 |  [11. Graph analysis](https://www.udacity.com/course/viewer#!/c-ud758/l-6374209610/m-6842807731), [12. Dimensionality Reduction](https://www.udacity.com/course/viewer#!/c-ud758/l-6334098665)                  | [Spark GraphX](/spark/spark-graphx.html)                                 |   Project Proposal Due (Oct 14)                                                                        | 
| 9      | Oct 15-19  |  [13. Patient similairty](https://www.udacity.com/course/viewer#!/c-ud758/l-6375269344/m-6857168643), [14. DNN](http://sunlab.org/teaching/cse6250/fall2018/dl/dl-fnn.html#artificial-neural-networks)        |   [Deep Learning Lab](/dl/dl-setup.html)                               |       HW4 Due (Oct 21)                                                                   | 
| 10     | Oct 22-26 |   [15. CNN](http://sunlab.org/teaching/cse6250/fall2018/dl/dl-cnn.html), [16. RNN](http://sunlab.org/teaching/cse6250/fall2018/dl/dl-rnn.html#recurrent-neural-networks-2)               | |                                                                          | 
| 11     | Oct 29 - Nov 2 |   Potential Guest Lecture                                    |                                  |                                                                           HW5 Due (Nov 4)
| 12     | Nov 5-9 |  Potential Guest Lecture                                                 |                                     |                                   | 
| 13     | Nov 12-16 |  Potential Guest Lecture                                    |                                  |                                                Project Draft Due (Nov 11)                          | 
| 14     | Nov 19-23 |  Project Discussion                                    |                                  |                                                                          | 
| 15     | Nov 26-30 |  Project Discussion                                     |                                  |                                                                          | 
| 16     | Dec 3-7 |   Final Exam Week                                    |                                  | Final Project Due (code + presentation + final paper) (Dec 9) | 

 <!-- **Guest lecture by Noemie Elhadad (Columbia)** -->
 
 <!--| Week # | Dates     | In-class lesson                                                     | Video lessons                       | Lab                              | Deliverable Due                                                          | 
|--------|-----------|---------------------------------------------------------------------|-------------------------------------|----------------------------------|--------------------------------------------------------------------------| 
| 1      | 8/21/2018  | Intro to the BDH class                                              | [1. Intro to Big Data Analytics](https://www.udacity.com/course/viewer#!/c-ud758/l-6311012028)      |                                  |                                                                          | 
| 1      | 8/23/2018 | Sunlab's research by Jimeng Sun                                     | [2. Course Overview](https://www.udacity.com/course/viewer#!/c-ud758/l-5046828066)                  | [Scala Basic](/spark/scala-basic.html)                      |                                                                          | 
| 2      | 8/28/2018 | Deep learning for healthcare by Edward Choi                         | [3. Predictive Modeling](https://www.udacity.com/course/viewer#!/c-ud758/l-5484251492)              |                                  |                                                                          | 
| 2      | 8/30/2018 | **Guest lecture by Mark Braunstein**                                    |                                     |                                  |  HW1 (9/2/2018)                                                                        |
| 3      | 9/4/2018 | Deep learning for healthcare by Edward Choi - Cont.                 | [4.MapReduce](https://www.udacity.com/course/viewer#!/c-ud758/l-6298155413)& [HBase](/hadoop/hadoop-hbase.html)                  |                                  |                                                           | 
| 3      | 9/6/2018 |                                                                     |                                     | [Hadoop & HDFS Basics](/hadoop/hdfs-basic.html)             |                                                                          | 
| 4      | 9/11/2018 | **Guest lecture by Chunhua Wen (Columbia)**                             | [5.Classification evaluation metrics](https://www.udacity.com/course/viewer#!/c-ud758/l-5505090946) |                                  |                                                                          | 
| 4      | 9/13/2018  |                                                                     | [6.Classification ensemble methods](https://www.udacity.com/course/viewer#!/c-ud758/l-5615268587)   | [Hadoop Pig](/hadoop/hadoop-pig.html) & [Hive](/hadoop/hadoop-hive.html)                |                                                                          | 
| 5      | 9/18/2018  | **Guest lecture by Jon Duke**                                           | [7. Phenotyping](https://www.udacity.com/course/viewer#!/c-ud758/l-6363218753)                      |                                  |                                                                          | 
| 5      | 9/20/2018  |                                                                     | [8. Clustering](https://www.udacity.com/course/viewer#!/c-ud758/l-6343118554)                       | "[Spark Basic](/spark/spark-basic.html), [Spark SQL](/spark/spark-sql.html) "         | HW2 (9/23/2018)                                                          | 
| 6      | 9/25/2018 | **Guest lecture by Rachel Patzer (Emory)**                              | [9. Spark](https://www.udacity.com/course/viewer#!/c-ud758/l-6376189383/m-6861062716)                            |                                  |                                                                          | 
| 6      | 9/27/2018 |                                                                     |                                     | [Spark Application](/spark/spark-application.html) & [Spark MLlib](/spark/spark-mllib.html)  |  Project Group Formation (9/30/2018)                                                                        | 
| 7      | 10/2/2018 | Computational phenotyping with tensor factorization by Kimis Perros (I)     | [10. Medical ontology](https://www.udacity.com/course/viewer#!/c-ud758/l-6370309670)                 |                                  |                                                                          | 
| 7      | 10/4/2018 |  |                                     |  [NLP Lab by Charity Hilton](/nlp/solr.html)      | HW3 (10/7/2018)                                                          | 
| 8      | 10/9/2018 | **Guest lecture by David Page (UW Madison)**                            | [11. Graph analysis](https://www.udacity.com/course/viewer#!/c-ud758/l-6374209610/m-6842807731)                  |                                  |                                                                          | 
| 8      | 10/11/2018  |                                                                     |                                     | [Spark GraphX](/spark/spark-graphx.html)                     |  Project Proposal (10/14/2018)                                       | 
| 9      | 10/16/2018  | **Guest lecture by Jim Rehg**                                           | [12. Dimensionality Reduction](https://www.udacity.com/course/viewer#!/c-ud758/l-6334098665)        |                                  |                                                                          | 
| 9      | 10/18/2018  |                                                                     |                                     | [Deep Learning Lab](/dl/dl-setup.html)  |                                             | 
| 10     | 10/23/2018 |                                                                | [13. Patient similairty](https://www.udacity.com/course/viewer#!/c-ud758/l-6375269344/m-6857168643)              | [Deep Learning Lab by Sungtae An - Cont.](/dl/dl-setup.html)    |                                                                          | 
| 10     | 10/25/2018 |                                                                     | [14. DNN]                                   |         | HW4 (10/28/2018)                                                          | 
| 11     | 10/30/2018 |                                                         | [15. CNN]                                    |                                  |                                                                          | 
| 11     | 11/1/2018 |                                                         | [16. RNN]                                    |                                  | | 
| 12     | 11/6/2018 | Project Discussion                                                  |                                     |                                  | | 
| 12     | 11/8/2018 | Project Discussion                                                  |                                     |                                  | Project Draft (11/11/2018)                                                 | 
| 13     | 11/13/2018  | Computational phenotyping with tensor factorization by Kimis Perros - Cont.   |                                     |                                  |                                                                          | 
| 13     | 11/15/2018  | **Guest lecture by Greg Cooper (UPitt)**                                |                                     |                                  |  HW5 (11/17/2018)                                        | 
| 14     | 11/20/2018 | **Guest**                                                               |                                     |                                  |                                                                          | 
| 14     | 11/22/2018 | **Guest lecture: S. Joshua Swamidass (Wash U.)**                        |                                     |                                  |                                                                          | 
| 15     | 11/27/2018 |                                                                |                                     |                                  |                                                                          | 
| 15     | 11/29/2018 | **Guest lecture by Walter 'Buzz' Stewart (Sutter Health)**    |                                     |                                  |                                                                          | 
| 16     | 12/4/2018 |                                                                |                                     |                                  |                                                                          | 
| 16     | 12/6/2018 |                                                                     |                                     |                                  | Final Project with code, presentation, and the final paper (12/9/2018) | 
-->

## Previous Guest Lectures

See [RESOURCE](/resource.html) section.
