---
layout: SpecialPage
---
# Resource

## Guest Lectures

One may find the recent guest lectures recorded at this [LINK](https://smartech.gatech.edu/handle/1853/59348) or this [LINK](https://cdnapisec.kaltura.com/p/2019031/sp/201903100/embedIframeJs/uiconf_id/40436601/partner_id/2019031/widget_id/1_2ipb2j47?iframeembed=true&playerId=kaltura_player_5aaa84d737fc5&flashvars[playlistAPI.kpl0Id]=1_pjxzalrg&flashvars[playlistAPI.autoContinue]=true&flashvars[playlistAPI.autoInsert]=true&flashvars[ks]=&flashvars[localizationCode]=en&flashvars[imageDefaultDuration]=30&flashvars[leadWithHTML5]=true&flashvars[forceMobileHTML5]=true&flashvars[nextPrevBtn.plugin]=true&flashvars[sideBarContainer.plugin]=true&flashvars[sideBarContainer.position]=left&flashvars[sideBarContainer.clickToClose]=true&flashvars[chapters.plugin]=true&flashvars[chapters.layout]=vertical&flashvars[chapters.thumbnailRotator]=false&flashvars[streamSelector.plugin]=true&flashvars[EmbedPlayer.SpinnerTarget]=videoHolder&flashvars[dualScreen.plugin]=true) with the following order of playlist.

1. **Stewart, Walter** from Sutter Health. *How Do We Accelerate Data Driven Health Care?* (April 19, 2018)
2. **Swamidass, S. Joshua** from Washington University in St. Louis. *Translating from Chemistry to Clinic with Deep Learning: Modeling the Metabolism and Subsequent Reactivity of Drugs* (April 12, 2018)
3. **Cooper, Gregory** from Univ. of Pittsburgh. *Causal Network Discovery from Biomedical and Clinical Data* (April 5, 2018)
4. **James M. Rehg** from School of Interactive Computing, GT. *Big Data in Behavioral Medicine*. (Mar 6, 2018)
5. **David Page** from Univ. of Wisconsin-Madison. *ML from EHR: High-Throughput Prediction, Insights into Causation*. (Feb 27, 2018)
6. **Ioakeim (Kimis) Perros** from SunLab. *Unsupervised Phenotyping using Tensor Factoriztion*. (Feb 20, 2018)
7. **Jon Duke** from Georgia Tech Research Institute. *Precision Medicine at Georgia Tech: Introduction to the Health Data Analytics Platform*. (Feb 6, 2018)
8. **Rachel Patzer** from Emory Univ. *Predicting Hospital Readmissions Among Kidney Transplant Recipients*. (Feb 13, 2018)
9. **Chunhua Weng** from Columbia Univ. *Using Electronic Health Records Data to Support Patient Care and Clinical Research*. (Jan 30, 2018)

## Past Guest Lectures

<!--<span style="color:red">**Find slides of past guest lectures in T-Square resources**</span>.-->

1. **Jon Duke** from Regenstrief Institute. *Phenotyping on OHDSI*. (Jan 24, 2017, in class) [[Video]](https://bluejeans.com/s/Obo6Y/)(Slides under T-square Resources)

2. **Jon Duke** from Regenstrief Institute. *NLP in Health Data Analytics*. (Jan 31, 2017, in class) [[Video]](https://bluejeans.com/s/q29hA/)

<!--3. **Bess Searles** from Children's Healthcare of Atlanta *Value Based Care and Population Health*. (Feb 7, 2017, in class) (Slides under T-square Resources)
-->

3. **Ben Snively** from Amazon Web Services (AWS). *Analytics on the Cloud*. (Jan 21, 2016, in class) [[Slide]](https://www.dropbox.com/s/wqzz5dl3avwfb6d/Ben%20Snively%20AWS_GT_BigData_Talk.pdf?dl=0)[[Video]](https://www.dropbox.com/s/zpfcqu5hsk62er6/012116_Ben_Snively_guest_speaker.mp4?dl=0)

4. **Richard Starr** from Institue for People and Technology (IPaT) @ Georgia Tech. *OHDSI (Observational Health Data Sciences and Informatics)*. (Spring 2016, in class) [[Video]](https://www.dropbox.com/s/klq5s9da0q4r2xs/Guest_Speaker_Richard_Starr.mp4?dl=0)

5. **Michael Thompson** from Children's Healthcare of Atlanta (CHOA). *Using Electronic Medical Records for Research*. (Apr 21, 2015, in class) [[Video]](https://www.dropbox.com/s/qz5e6d9du7zsfrk/JimengSun_Thompson_040715.mp4?dl=0)

6. **Jon Duke** from Regenstrief Institute. *Natural Language Processing in Health Data Analytics*. (Apr 2, 2015, in class) [[Video]](https://www.dropbox.com/s/3bpt8f153kjm0or/JimengSun_Jon_Duke_040215.mp4?dl=0)

7. **Gari Clifford** from Emory University. *Real-world Applications in Healthcare*. (Mar 31, 2015, in class) [[Video]](https://www.dropbox.com/s/bh00japskseqe7s/JimengSun_Clifford_033115.mp4?dl=0)

<!--
1. **Jon Duke** from Regenstrief. *Natural Language Processing for Deep Phenotyping in Health Data Analytics*. (Jan 20, TSRB)
2. **Ben Snively** from AWS. *Analytics on the Cloud*. (Jan 21, in class)
3. **[Joyce Ho](http://joyceho.github.io/)** from Emory, *Clinically interpretable models for health data*. (Mar 3, in class)
4. **[Yubin Park](https://sites.google.com/site/yubindata/)** from Accordion Health, *Building a Platform for Value Based Payment*. (Mar 8, in clas)
5. **[Shamim Nemati](http://www.bmi.emory.edu/Nemati)** from Emory, *ICU data analysis using deep learning*. (Mar 10, in class)
6. **[Omer Inan](https://www.ece.gatech.edu/faculty-staff-directory/omer-t-inan)** from Gatech ECE. *Topic TODO*. (Mar 17, in class)
4. **[Xiong Li](http://www.mathcs.emory.edu/~lxiong/)** from Emory, *Privacy in Medical data*.(Apr 5, in class)
3. **[Munmun De Choudhury](http://www.munmund.net/index.html)** from Gatech IC. *Social media and health*. (Apr 7, in class)
6. **[Mark Braunstein](http://www.ic.gatech.edu/people/mark-braunstein)** from Gatech IC. *Healthcare Interoperability*. (Apr 12, in class)
7. **[Jim Rehg](http://rehg.org/)** from Gatech IC. *Behavioral Imaging*. (Apr 14, in class)
7. **[Jim Rehg](http://rehg.org/)** from Gatech IC. *Disease Progression*. (Apr 21, in class)
-->
